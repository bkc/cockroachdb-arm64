ARG CRDB_VERSION=22.1.3

FROM golang:1.18-bullseye as prebuild
RUN go version
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt -y upgrade
RUN ["/bin/bash", "-c", "curl -sL https://deb.nodesource.com/setup_18.x | bash -"]
RUN curl https://bazel.build/bazel-release.pub.gpg | apt-key add
RUN echo "deb [arch=arm64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt -y update
RUN apt -y install build-essential gcc g++ cmake autoconf wget bison libncurses-dev ccache curl git libgeos-dev tzdata apt-transport-https lsb-release ca-certificates bazel* yarn nodejs

FROM prebuild as build
RUN /bin/bash -c "mkdir -p $(go env GOPATH)/src/github.com/cockroachdb && \
    cd $(go env GOPATH)/src/github.com/cockroachdb && \
	git clone --branch v21.2.4 https://github.com/cockroachdb/cockroach /go/src/github.com/cockroachdb/cockroach && \
	cd cockroach && \
	git submodule update --init --recursive && \
	make build && \
	make install"

FROM debian:bullseye
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt -y upgrade && apt install -y libc6 ca-certificates tzdata hostname tar && rm -rf /var/lib/apt/lists/*
WORKDIR /cockroach/
ENV PATH=/cockroach:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
RUN mkdir -p /cockroach/ /usr/local/lib/cockroach /licenses
COPY --from=build /usr/local/bin/cockroach /cockroach/cockroach
COPY --from=build /go/native/aarch64-linux-gnu/geos/lib/libgeos.so /go/native/aarch64-linux-gnu/geos/lib/libgeos_c.so /usr/local/lib/cockroach/
EXPOSE 26257 8080
CMD ["/cockroach/cockroach"]
